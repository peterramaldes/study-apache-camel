package com.br.studyapachecamel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyApacheCamelApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudyApacheCamelApplication.class, args);
	}

}
